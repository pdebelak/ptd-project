;;; ptd-project.el --- A package for running project-aware commands

;; Copyright 2018 Peter Debelak <pdebelak@gmail.com>

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:

;; Meant as a personal replacement for projectile.el with greatly
;; pared down functionality.

;;; Code:
(require 'compile)
(require 'grep)
(require 'ido)
(require 'seq)

(defgroup ptd-project nil
  "Options ptd-project plugin"
  :group 'tools)

(defcustom ptd-project-root-files
  '(".git"
    "Makefile"
    "pom.xml"
    "build.sbt"
    "gradlew"
    "build.gradle"
    ".ensime"
    "Gemfile"
    "requirements.txt"
    "setup.py"
    "Cargo.toml"
    "mix.exs"
    "TAGS"
    "GTAGS"
    "configure.in"
    "configure.ac"
    )
  "A list of files to mark the root of a project.

Used by \\[ptd-project-root]."
  :group 'ptd-project
  :type '(repeat string))

(defcustom ptd-project-git-files-command
  "git ls-files -co --exclude-standard"
  "Command used to get list of files in git project."
  :group 'ptd-project
  :type 'string)

(defcustom ptd-project-files-command
  "find . -type f"
  "Command used to get list of files in non-git project."
  :group 'ptd-project
  :type 'string)

(defcustom ptd-project-git-grep-template
  "git --no-pager grep --untracked -ni -e <R>"
  "Grep template to use in git project."
  :group 'ptd-project
  :type 'string)

(defcustom ptd-project-grep-template
  "find . -type f -exec grep  -nH --null -e <R> \{\} +regexp"
  "Grep template to use in non-git project."
  :group 'ptd-project
  :type 'string)

(defcustom ptd-project-files-by-name-template
  "find . -name <R>"
  "Grep template to use to find a file by name."
  :group 'ptd-project
  :type 'string)

(defcustom ptd-project-find-file-min-smart-match
  4
  "Minimum query length to enable smart matching in `ptd-project-find-file'."
  :group 'ptd-project
  :type 'integer)

(defun ptd-project-root ()
  "Find root of project, if exists.

Uses `ptd-project-root-files' for list of files to locate root."
  (let ((root
         (seq-find
          (lambda (file) (locate-dominating-file default-directory file))
          ptd-project-root-files)))
    (when root
      (file-name-directory
       (expand-file-name (locate-dominating-file default-directory root))))))

(defun ptd-project-p ()
  "Whether we are in a project or not."
  (if (ptd-project-root) t nil))

(defmacro ptd-project-in-project-root (select &rest body)
  "Run BODY inside project root, if available.

When SELECT is non-nil prompt for directory starting with default."
  `(let ((starting-directory
         (if (ptd-project-p)
             (ptd-project-root)
           default-directory)))
    (if ,select
        (let ((default-directory
                 (read-directory-name "From directory: " starting-directory)))
           ,(cons 'progn body))
      (let ((default-directory starting-directory))
        ,(cons 'progn body)))))

(defun ptd-project-git-p ()
  "Determine whether a project is a git repo or not."
  (if (locate-dominating-file default-directory ".git")
      t
    nil))

(defun ptd-project--find-files ()
  "Shell out to find to get files.

Assumes `default-directory' has been set to project root."
  (split-string (shell-command-to-string ptd-project-files-command) "\n" t))

(defun ptd-project--git-ls-files ()
  "Shell out to git ls-files to get files.

Assumes `default-directory' has been set to project root."
  (split-string (shell-command-to-string ptd-project-git-files-command) "\n" t))

(defun ptd-project-files ()
  "List files in project.

Assumes `default-directory' has been set to project root."
  (if (ptd-project-git-p)
      (or (ptd-project--git-ls-files) (ptd-project--find-files))
    (ptd-project--find-files)))

(defun ptd-project-ido-regexp (str)
  "Turn STR into fuzzy regexp for ido matching."

  (mapconcat
   (lambda (c)
     (let ((quoted-char (regexp-quote (char-to-string c))))
       (concat "[^" quoted-char "]*" quoted-char)))
   str ""))

(defun ptd-project-ido-match-item (regexp item &optional did-match start end match-length)
  "Match REGEXP against ITEM.

DID-MATCH START END and MATCH-LENGTH are interal recursion
trackers.

Returns list of (ITEM DID-MATCH START MATCH-LENGTH)."
  (let* ((cur-end (or end 0))
         (this-start (string-match regexp (substring item (or end 0))))
         (this-end (if this-start (match-end 0)))
         (this-length (if this-start (- this-end this-start)))
         (which-better
          (cond ((not match-length) 'this)
                ((not this-length) 'that)
                ((< match-length this-length) 'that)
                (t 'this))))
    (if this-start
        (ptd-project-ido-match-item
         regexp
         item
         t
         (if (eq which-better 'this) (+ cur-end this-start) start)
         (+ cur-end this-end)
         (if (eq which-better 'this) this-length match-length))
      (list item did-match start match-length))))

(defun ptd-project-ido-match-matches (query items)
  "Find match data for ITEMS using QUERY."

  (let* ((regexp (ptd-project-ido-regexp query))
         (all-matches (mapcar (lambda (item) (ptd-project-ido-match-item regexp item)) items))
         (matches (seq-filter (lambda (match) (cadr match)) all-matches)))
    (mapcar
     'car
     (sort
      matches
      (lambda (first second)
        (or
         (< (cadddr first) (cadddr second))
         (and
          (= (cadddr first) (cadddr second))
          (< (caddr first) (caddr second)))))))))

(defun ptd-project-ido-match (orig-fun &rest args)
  "Run ORIG-FUN or `ptd-project-ido-match-matches' on ARGS."

  (let ((query ido-text)
        (items (apply orig-fun args)))
    (if (< (length query) ptd-project-find-file-min-smart-match)
        items
      (ptd-project-ido-match-matches query items))))

(defun ptd-project-find-file (select)
  "Find file in project.

If SELECT is non-nil prompt for directory to locate files in."
  (interactive "P")
  (ptd-project-in-project-root
   select
   (unwind-protect
       (progn
         (advice-add 'ido-set-matches-1 :around #'ptd-project-ido-match)
         (find-file (ido-completing-read "Find file: " (ptd-project-files))))
     (advice-remove 'ido-set-matches-1 #'ptd-project-ido-match))))

(defvar ptd-project-compile-current-command ""
  "Stores command value to be used in \\[ptd-project-compile].")
(defun ptd-project-compile (select)
  "Run compile with comint in project root.

If SELECT is non-nil prompt for directory to compile in."
  (interactive "P")
  (ptd-project-in-project-root select
   (let ((command
          (read-shell-command "Run program: " ptd-project-compile-current-command 'compile-history)))
     (compile command t))))

(defun ptd-project-grep (select)
  "Run grep command in project root.

If SELECT is non-nil prompt for directory to grep in."
  (interactive "P")
  (grep-compute-defaults)
  (ptd-project-in-project-root
   select
   (let ((regexp (grep-read-regexp))
         (grep-template
          (if (ptd-project-git-p)
              ptd-project-git-grep-template
            ptd-project-grep-template)))
     (grep (grep-expand-template grep-template regexp)))))

(defun ptd-project-file-grep (select)
  "Find files by name and place in grep buffer.

If SELECT is non-nil prompt for directory to searchi n."
  (interactive "P")
  (grep-compute-defaults)
  (ptd-project-in-project-root
   select
   (let ((regexp (grep-read-regexp))
         (grep-template
          (format "%s | sed $'s/$/:1:/' | sed $'s/^.\\///'" ptd-project-files-by-name-template)))
     (grep (grep-expand-template grep-template regexp)))))

(defun ptd-project-link-to-git-forge (filename linenum)
  "Copies link to FILENAME and LINENUM in appropriate git-forge to kill ring."
  (interactive (list (buffer-file-name) (line-number-at-pos)))
  (ptd-project-in-project-root
   nil
   (if (ptd-project-git-p)
       (let ((branch-name (shell-command-to-string "git branch --no-color 2>/dev/null | awk '$1 == \"*\" { printf $2 }'"))
             (remotes (split-string (shell-command-to-string "git remote") "\n" t)))
         (let ((remote (if (= (length remotes) 1) (car remotes) (ido-completing-read "Remote: " remotes))))
           (let ((remote-url
                  (string-trim-right
                   (shell-command-to-string
                    (concat
                     "git remote -v | awk '$1 == \""
                     remote
                     "\" { print $2 }' | head -n 1 | sed -e 's|:|/|' -e 's|.*@|https://|' -e 's/\.git$//'")))))
             (let ((forge-link (concat remote-url "/tree/" branch-name "/" (string-remove-prefix default-directory filename) "#L" (number-to-string linenum))))
               (message forge-link
               (kill-new forge-link)))))))))

(provide 'ptd-project)
;;; ptd-project.el ends here
