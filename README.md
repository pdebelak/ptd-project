# ptd-project

Meant as a personal replacement for projectile.el with greatly pared
down functionality. It eliminates some things projectile has that I
don't use but slow it down with the trade off that it does far less
and is presumably less robust.

## Commands

`M-x ptd-project-file-file` to find a file in your project.

`M-x ptd-project-compile` to run a compilation command in the project root.

`M-x ptd-project-grep` to run aa search in your project.

All of these commands work outside a project as well but may be slower.

## Customization

This is setup to work out of the box for anyone using git. If you want
to, for example, use [ripgrep](https://github.com/BurntSushi/ripgrep),
try the following customizations:

1. Set `ptd-project-files-command` to `"rg --files"`.
2. Set `ptd-project-git-files-command` to `"rg --files"`.
3. Set `ptd-project-git-grep-template` to `"rg --vimgrep --no-heading -S <R>"`.
4. Set `ptd-project-grep-template` to `"rg --vimgrep --no-heading -S <R>"`.

Use `M-x customize-group <RET> ptd-project` to change these values.
